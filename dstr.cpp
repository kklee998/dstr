/*
Lee Kah Kin (TP041827)
Compiled successfully with g++ (Ubuntu 8.2.0-7ubuntu1) 8.2.0 on
Ubuntu 18.10 x86_64, Kernel: 4.18.0-13-generic
source file can be found on https://bitbucket.org/kklee998/dstr/src/master/
 */

#include <iostream>

using namespace std;

/*

https://www.geeksforgeeks.org/sparse-matrix-representation/
0123456
*012345
**01234
***0123
****012
*****01
******0


01234
*0123
**012
***01
****0

012
*01
**0

*/

struct Node {
    int data, x ,y;
    Node *next;
};

class UpperTriangularMatrix
{

    public:
        int row,col;
        Node *head, *tail;
        //constructor
        //https://www.tutorialspoint.com/cplusplus/cpp_constructor_destructor.htm
        UpperTriangularMatrix(int row, int col){
            this->row = row;
            this->col = col;
            this->head = NULL;
            this->tail = NULL;

            for (int x = 0; x < row; x++){
                for (int y = 0; y < col; y++) {
                    if(y > x - 1) {
                        //only create nodes that are in the UTM position
                        newNode(y,x,0); //Coordinates are included in the node
                    }
                }
            }
        }
        //destructor
        //https://www.tutorialspoint.com/cplusplus/cpp_constructor_destructor.htm
        ~UpperTriangularMatrix() {
            delete head,tail; //deallocates memory after program is closed.

        }

        void newNode(int x, int y, int data){
            //creating new node
            //https://www.geeksforgeeks.org/linked-list-set-1-introduction/
            Node *temp = new Node;
            temp->next = NULL;
            temp->x = x;
            temp->y = y;

            if(head==NULL){
                head = temp;
                tail = temp;
            }else{
                tail->next = temp;
                tail = temp;
            }
        }

        void addData(int x, int y, int data){
            //adding data into the node

            Node *current = new Node;
            Node *next_node = new Node;

            current = head;
            next_node = head->next;

            while(current != NULL){
                if (current->x == x && current->y == y){
                    //checks if node coordinates to verify if it's in UTM position
                    current->data = data;
                    current->next = next_node;
                    current = NULL;
                }else{
                    //check the next node
                    current = next_node;
                    next_node = current->next;
                }
            }

        }

        int readData(int x, int y){

            //read data from the node
            Node *current = new Node;
            Node *next_node = new Node;

            current = head;
            next_node = head->next;

            while(current != NULL){
                if (current->x == x && current->y == y){
                    return current->data;
                }else{
                    current = next_node;
                    next_node = current->next;
                }
            }

            return 0;

        }

        void displayList() {
            // https://www.geeksforgeeks.org/linked-list-set-1-introduction/
            // display data as a linked list

            Node *node = new Node;
            node = head;
            while(node != NULL) {
                cout << node->data <<"  ";

                if(node == tail){
                    break;
                }

                node = node->next;
            }

        }
        void displayMatrix() {
            //display the data as a matrix
            //https://www.geeksforgeeks.org/program-print-lower-triangular-upper-triangular-matrix-array/
            for (int x = 0; x < row; x ++) {
                for (int y = 0; y < col; y++) {
                    if(y <= x - 1) {
                        cout << "0" << "\t"; //Zero elements
                    }else{
                        cout << readData(y, x) << "\t"; //UTM elements from the nodes
                    }if(y == col - 1) {
                        cout << endl << endl;
                    }
                }
            }

        }
};

void start();
void menu(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2);
void fill_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2);
void print_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2);
void sum_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2);

void start()
{
    //Forces all UTM to be same square size
    int size;
    cout << "Please choose the size of your upper triangle matrix (only squares allow) : ";
    cin >> size;
    //initialises both UTMs
    UpperTriangularMatrix utm1 = UpperTriangularMatrix(size, size);
    UpperTriangularMatrix utm2 = UpperTriangularMatrix(size, size);

    cout<<"2 UPPER TRIANGLE MATRIX are created with size " << size << endl;
    cout << "PRESS ANY KEY TO CONTINUE" << endl;
    cin.ignore(1024, '\n'); // https://ubuntuforums.org/showthread.php?t=1145685 for pausing without bugs
    cin.get();
    menu(utm1, utm2);
}

void menu(UpperTriangularMatrix utm1, UpperTriangularMatrix utm2)
{
    system("clear");
    int choice;
    cout << "****************************************************";
    cout << "\n*                                                  *";
    cout << "\n*                                                  *";
    cout << "\n*                     MAIN MENU                    *";
    cout << "\n*                                                  *";
    cout << "\n*                                                  *";
    cout << "\n****************************************************\n";

    cout << "1) FILL IN UTM" << endl;
    cout << "2) PRINT UTM" << endl;
    cout << "3) Add 2 UTM" << endl;
    cout << "7) RESTART" << endl;
    cout << "8) Exit" << endl;
    cin >> choice;

    switch(choice){
        case 1:
            fill_utm(utm1,utm2);
        case 2:
            print_utm(utm1,utm2);
        case 3:
            sum_utm(utm1,utm2);
        case 7:
            start();
        case 8:
            exit(1);
        default:
            cout <<"Please select a valid number!!!" << endl;
            cout << "PRESS ANY KEY TO CONTINUE" << endl;
            cin.ignore(1024, '\n'); // https://ubuntuforums.org/showthread.php?t=1145685 for pausing without bugs
            cin.get();
            menu(utm1,utm2);
    }

}

void fill_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2)
{
    int data;
    system("clear");
    cout << "FILL IN THE MATRIX 1" << endl;

    for(int x = 0; x < utm1.row; x++){
        for(int y = 0; y < utm1.col; y++) {
            if(y > x - 1) {
                cout << "Please enter a number: ";
                cin >> data;
                while (data <= 0) {
                    //only allow non-negative larger than zero elements
                    cout << endl << "Only enter numbers larger than zeros :";
                    cin >> data;

                }
                utm1.addData(y,x,data);
            }
        }
    }
    cout << endl << "THE END" << endl;
    cin.ignore(1024, '\n'); // https://ubuntuforums.org/showthread.php?t=1145685 for pausing without bugs
    cin.get();
    system("clear");

    cout << "==MATRIX ONE==" <<endl << endl;
    utm1.displayMatrix();
    cout  << endl << "==MATRIX ONE==" <<endl;
    cout << "PRESS ANY KEY TO CONTINUE";
    cin.get();

    system("clear");
    cout << "FILL IN THE MATRIX 2" << endl;

    for(int x = 0; x < utm2.row; x++){
        for(int y = 0; y < utm2.col; y++) {
            if(y > x - 1) {
                cout << "Please enter a number: ";
                cin >> data;
                while (data <= 0) {
                    //only allow non-negative larger than zero elements
                    cout << endl << "Only enter numbers larger than zeros :";
                    cin >> data;

                }
                utm2.addData(y,x,data);
            }
        }
    }
    cout << endl << "THE END" << endl;
    cin.ignore(1024, '\n');
    cin.get();
    system("clear");

    cout << "==MATRIX TWO==" <<endl << endl;
    utm2.displayMatrix();
    cout  << endl << "==MATRIX TWO==" <<endl;
    cout << "PRESS ANY KEY TO CONTINUE";
    // cin.ignore(1024, '\n');
    cin.get();
    menu(utm1, utm2);

}

void print_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2)
{

    system("clear");
    cout << "==MATRIX ONE==" <<endl << endl;
    utm1.displayMatrix();
    cout  << endl << "==MATRIX ONE==" <<endl << endl;

    cout<< "==LINKED LIST ONE==" <<endl << endl;
    utm1.displayList();
    cout << endl << endl << "==LINKED LIST ONE==" << endl << endl;
    cout << "PRESS ANY KEY TO CONTINUE" << endl;
    cin.ignore(1024, '\n');
    cin.get();

    system("clear");

    cout << "==MATRIX TWO==" <<endl << endl;
    utm2.displayMatrix();
    cout  << endl << "==MATRIX TWO==" <<endl << endl;
    cout<< "==LINKED LIST TWO==" <<endl << endl;
    utm2.displayList();
    cout << endl << endl << "==LINKED LIST TWO==" << endl << endl;
    cout << "PRESS ANY KEY TO CONTINUE" << endl;
    cin.get();
    menu(utm1, utm2);
}

void sum_utm(UpperTriangularMatrix utm1,UpperTriangularMatrix utm2)
{
    //sums and prints UTM
    system("clear");
    int sum;
    UpperTriangularMatrix utm_sum = UpperTriangularMatrix(utm1.row,utm1.col);

    cout << "ADDING UTM1 AND UTM2 TOGETHER!" << endl;

    for(int x = 0; x < utm1.row; x++){
        for(int y=0;y < utm1.row;y++){
            if(y > x -1){
                sum = utm1.readData(y,x) + utm2.readData(y,x);
                utm_sum.addData(y,x,sum);

            }
        }
    }

    cin.ignore(1024, '\n');
    cout << "PRESS ANY KEY TO CONTINUE" << endl;
    cin.get();
    //printing the summed up UTM

    cout << "==MATRIX THREE==" <<endl << endl;
    utm_sum.displayMatrix();
    cout  << endl << "==MATRIX THREE==" <<endl << endl;
    cout<< "==LINKED LIST THREE==" <<endl << endl;
    utm_sum.displayList();
    cout << endl << endl << "==LINKED LIST THREE==" << endl << endl;
    cout << "PRESS ANY KEY TO CONTINUE" << endl;
    cin.get();
    menu(utm1, utm2);

}

int main ()
{
    start();
    return 0;

}
